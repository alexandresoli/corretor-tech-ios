//
//  PostCell.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UIImageView *imagem;
@property(nonatomic,weak) IBOutlet UILabel *titulo;
@property(nonatomic,weak) IBOutlet UILabel *data;;

@end

//
//  NSFileManager+DoNotBackup.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 5/28/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "NSFileManager+DoNotBackup.h"

@implementation NSFileManager (DoNotBackup)


- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{

    NSError *error = nil;
//    id flag = nil;
//    BOOL excluded = [URL getResourceValue:&flag forKey:NSURLIsExcludedFromBackupKey error:&error];

//    if (!excluded) {
        [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
//    }
    

    return error == nil;
}



@end

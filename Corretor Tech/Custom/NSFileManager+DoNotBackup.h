//
//  NSFileManager+DoNotBackup.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 5/28/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (DoNotBackup)

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;


@end

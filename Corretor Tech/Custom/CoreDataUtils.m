//
//  CoreDataUtils.m
//  CatalogoLeisANS
//
//  Created by Alexandre Oliveira on 11/24/11.
//  Copyright (c) 2011 BRQ IT Services. All rights reserved.
//

#import "CoreDataUtils.h"

@implementation NSString (FetchedGroupByString)

- (NSString *)stringGroupByFirstInitial
{
    NSString *temp = [self uppercaseString];
    
    // Remove acentuação para retornar a letra sem acento correspondendo a ordem dos objetos retornados no NSFetchedResultsController.
    NSData *asciiEncoded = [temp dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    temp = [[NSString alloc] initWithData:asciiEncoded
                                 encoding:NSASCIIStringEncoding];
    
    if (!temp.length || temp.length == 1)
        return self;
    return [temp substringToIndex:1];
}

@end

@implementation CoreDataUtils

// An object that may be converted to JSON must have the following properties:
//
// The top level object is an NSArray or NSDictionary.
// All objects are instances of NSString, NSNumber, NSArray, NSDictionary, or NSNull.
// All dictionary keys are instances of NSString.
// Numbers are not NaN or infinity.
//  Default returns a Array of NSManagedObjects.

+ (NSArray *)fetchEntityAsDictionary:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.resultType = NSDictionaryResultType;
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError* error;
    
    return  [moc executeFetchRequest:request error:&error];
}



+ (NSArray *)fetchEntity:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];    
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError* error;

    return  [moc executeFetchRequest:request error:&error];
}

+ (NSArray *)fetchEntity:(NSString *)entityName  withPredicate:(NSPredicate *)predicate withKey:(NSString *)key withOrder:(BOOL)order inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:order];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];

    NSError* error;
    
    return  [moc executeFetchRequest:request error:&error];
}


+ (NSArray *)fetchEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate withKey:(NSString *)key withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    if (limit > 0) {
        [request setFetchLimit:limit];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:order];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError* error;
    
    return  [moc executeFetchRequest:request error:&error];
}


+ (NSArray *)fetchEntityID:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];    
    [request setIncludesPropertyValues:YES];
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError* error;
    
    return  [moc executeFetchRequest:request error:&error];
}

+ (long)countEntity:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];    
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError* error;
    NSUInteger count;
    
    count = [moc countForFetchRequest:request error:&error];
    
    return  count;
}


+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity:(NSString *)entityName andKey:(NSString *)key withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    if (limit > 0) {
        [fetchRequest setFetchLimit:limit];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:order];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
        
    [fetchRequest setSortDescriptors:sortDescriptors];

    
    NSFetchedResultsController *fetchedResultsController;
    
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:key cacheName:nil];
    
    NSError* error;
    
    [fetchedResultsController performFetch:&error];
    
    return fetchedResultsController;
}



+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity2:(NSString *)entityName andKey:(NSString *)key withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    if (limit > 0) {
        [fetchRequest setFetchLimit:limit];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:order];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *fetchedResultsController;
    
    if (key == nil) {
        
        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:nil cacheName:nil];
    } else {
        
        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:@"sectionIdentifier" cacheName:nil];
    }
    
    NSError* error;
    
    [fetchedResultsController performFetch:&error];
    
    return fetchedResultsController;
}



+ (NSFetchedResultsController *)fetchedResultsControllerWithAlphabetic:(NSString *)entityName andKey:(NSString *)key withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    if (limit > 0) {
        [fetchRequest setFetchLimit:limit];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    key = [key stringByAppendingString:@".stringGroupByFirstInitial"];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:moc
                                                                            sectionNameKeyPath:key
                                                                                     cacheName:nil];
    
    NSError* error;
    
    [fetchedResultsController performFetch:&error];
    
    return fetchedResultsController;
}


+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity:(NSString *)entityName andKey1:(NSString *)key1 andKey2:(NSString *)key2 withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    if (limit > 0) {
        [fetchRequest setFetchLimit:limit];
    }
    
    
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:key1 ascending:order];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:key2 ascending:order];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:nil cacheName:nil];
    
    NSError* error;
    
    [fetchedResultsController performFetch:&error];
    
    return fetchedResultsController;
}



@end

//
//  Updater.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "Updater.h"

#import "Autor.h"
#import "Categoria.h"
#import "Tag.h"
#import "Post.h"
#import "DateUtils.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"

@interface Updater ()

@end

@implementation Updater


+ (BOOL)hasNewData:(NSArray *)json withMoc:(NSManagedObjectContext *)moc
{
    
    NSDictionary *posts = [json valueForKey:@"posts"];
    
    for(NSDictionary *obj in posts) {
        
        long count = [CoreDataUtils countEntity:@"Post" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[obj valueForKey:@"id"]] inContext:moc];
        
        if(count == 0) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Json Conversion

+ (int)convert:(NSArray *)json withMoc:(NSManagedObjectContext *)moc
{
    int newPosts = 0;
    NSDictionary *posts = [json valueForKey:@"posts"];
    
    for (NSDictionary *obj in posts) {
        Post *post;
        
        post = [[CoreDataUtils fetchEntity:@"Post" withPredicate:[NSPredicate predicateWithFormat:@"(categoria.descricao != 'Vídeo' AND categoria.descricao != 'Video') AND codigo = %@",[obj valueForKey:@"id"]] inContext:moc] firstObject];
        
        if (!post) {
            newPosts++;
        }
        
        [self dictionaryToPost:obj forPost:post withMoc:moc];
    }
    
    NSError *error = nil;
    [moc save:&error];
    
    return newPosts;
}


#pragma mark - Dictionary to Post

+ (void)dictionaryToPost:(NSDictionary *)obj forPost:(Post *)post withMoc:(NSManagedObjectContext *)moc
{
    if (!post) {
        post = [NSEntityDescription insertNewObjectForEntityForName:@"Post" inManagedObjectContext:moc];
        
    } else {
        NSDate *modifiedDate = [DateUtils dateFromString:[obj valueForKey:@"modified"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        if ([post.modificado isEqualToDate:modifiedDate]) {
            return;
        }
    }
    
    post.codigo = [obj valueForKey:@"id"];
    post.conteudo = [obj valueForKey:@"content"];
    post.data = [DateUtils dateFromString:[obj valueForKey:@"date"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
    post.modificado = [DateUtils dateFromString:[obj valueForKey:@"modified"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
    post.resumo = [obj valueForKey:@"excerpt"];
    post.status = [obj valueForKey:@"status"];
    post.titulo = [obj valueForKey:@"title"];
    post.url = [obj valueForKey:@"url"];
    
    if([obj valueForKey:@"thumbnail"] != (id)[NSNull null]){
        NSURL *url = [NSURL URLWithString:[[obj valueForKey:@"thumbnail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        post.thumbnail = [NSData dataWithContentsOfURL:url];
    }
    
    NSString *strUrl = [[[[obj valueForKey:@"thumbnail_images"] valueForKey:@"medium"] valueForKey:@"url"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    post.imagem = [NSData dataWithContentsOfURL:url];
    
    NSDictionary *author = [obj valueForKey:@"author"];
    
    long count = [CoreDataUtils countEntity:@"Autor" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[author valueForKey:@"id"]] inContext:moc];
    
    if(count > 0) {
        post.autor = [[CoreDataUtils fetchEntity:@"Autor" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[author valueForKey:@"id"]] inContext:moc] firstObject];
    } else {
        
        Autor *autor = [NSEntityDescription insertNewObjectForEntityForName:@"Autor" inManagedObjectContext:moc];
        autor.codigo = [author valueForKey:@"id"];
        autor.descricao = [author valueForKey:@"description"];
        autor.nome = [author valueForKey:@"name"];
        autor.url = [author valueForKey:@"url"];
        post.autor = autor;
    }
    
    
    NSDictionary *categories = [obj valueForKey:@"categories"];
    
    for(NSDictionary *dic in categories) {
        
        count = [CoreDataUtils countEntity:@"Categoria" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[dic valueForKey:@"id"]] inContext:moc];
        
        if(count > 0) {
            post.categoria = [[CoreDataUtils fetchEntity:@"Categoria" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[dic valueForKey:@"id"]] inContext:moc] firstObject];
        } else {
            
            Categoria *categoria = [NSEntityDescription insertNewObjectForEntityForName:@"Categoria" inManagedObjectContext:moc];
            categoria.codigo = [dic valueForKey:@"id"];
            categoria.descricao = [dic valueForKey:@"title"];
            categoria.contador = [dic valueForKey:@"post_count"];
            post.categoria = categoria;
        }
        
    }
    
    NSArray *tags = [obj valueForKey:@"tags"];
    
    for(NSDictionary *dic in tags) {
        
        long count = [CoreDataUtils countEntity:@"Tag" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[dic valueForKey:@"id"]] inContext:moc];
        
        Tag *tag = nil;
        
        if(count > 0) {
            tag = [[CoreDataUtils fetchEntity:@"Tag" withPredicate:[NSPredicate predicateWithFormat:@"codigo = %@",[dic valueForKey:@"id"]] inContext:moc] firstObject];
        } else {
            
            tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:moc];
            tag.codigo = [dic valueForKey:@"id"];
            tag.descricao = [dic valueForKey:@"title"];
            tag.contador = [dic valueForKey:@"post_count"];
        }
        
        [post addTagsObject:tag];
    }
    
}

#pragma mark - Check New Posts
+ (void)checkPosts:(NSManagedObjectContext *)moc
{
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Carregando posts" maskType:SVProgressHUDMaskTypeBlack];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://www.corretortech.com.br/?json=get_recent_posts&page=1&count=20" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self convert:responseObject withMoc:moc];
            
            [SVProgressHUD dismiss];
        });
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        [SVProgressHUD dismiss];
    }];
}


@end

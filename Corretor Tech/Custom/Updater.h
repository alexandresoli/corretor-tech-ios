//
//  Updater.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Updater : NSObject

+ (int)convert:(NSArray *)json withMoc:(NSManagedObjectContext *)moc;
+ (void)checkPosts:(NSManagedObjectContext *)moc;
+ (BOOL)hasNewData:(NSArray *)json withMoc:(NSManagedObjectContext *)moc;
@end

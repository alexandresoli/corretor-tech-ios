//
//  PostViewController.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "PostViewController.h"
#import "Post.h"
#import "Categoria.h"
#import "MailViewController.h"
#import <FacebookSDK/FacebookSDK.h>
@import Social;

@interface PostViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *htmlBody;
@property (nonatomic, strong) MailViewController *mailController;

@end

@implementation PostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *imagem = [UIImage imageWithData:self.selectedPost.imagem];
    
    NSString *imageHtml = @"";
    if(imagem != nil) {
        imageHtml = [self htmlForPNGImage:imagem];
    }
    
    self.htmlBody = [NSString stringWithFormat:@"<html> \n"
                          "<head> \n"
                          "<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.5; user-scalable=1'/> \n"
                          "<link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" /> \n"
                          "</head> \n"
                          "<body\"> <h1 class='article__title  article__title--single' style='font-family:Arial, Helvetica, sans-serif;font-weight:100;' itemtype='name'>%@</h1> %@ <br><br>  %@ <br></body> \n"
                          "</html>", self.selectedPost.titulo, imageHtml,  self.selectedPost.conteudo];
    
  //  self.htmlBody = [self.htmlBody stringByReplacingOccurrencesOfString:@"iframe width=\"420\"" withString:@"iframe width=\"300\""];
    
    NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    
    [self.webView loadHTMLString:self.htmlBody baseURL:baseURL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Utility Methods
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
- (NSString *) htmlForPNGImage:(UIImage *) image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *imageSource = [NSString stringWithFormat:@"data:image/png;base64,%@",[imageData base64Encoding]];
    return [NSString stringWithFormat:@"<img src='%@' />", imageSource];
}
#pragma GCC diagnostic pop


#pragma mark - UIWebViewDelegate
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}


#pragma mark - Reload
- (IBAction)reload:(id)sender
{
    CGPoint top = CGPointMake(0, -64);
    [self.webView.scrollView setContentOffset:top animated:YES];
}

#pragma mark - Twitter Share
- (IBAction)twitterShare:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *text = [NSString stringWithFormat:@"%@ - %@ @Corretor_tech @Br_estate",self.selectedPost.titulo,self.selectedPost.url];
        [tweetSheet setInitialText:text];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

#pragma mark - Send Email
- (IBAction)emailShare:(id)sender
{
    self.mailController = [[MailViewController alloc] init];
    [self.mailController sendEmail:self.selectedPost forSender:self withBody:self.htmlBody];
    
}

#pragma mark - Facebook Share
- (IBAction)facebookShare:(id)sender
{
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.name = @"Corretor Tech";
    params.caption = self.selectedPost.titulo;
    params.link = [NSURL URLWithString:self.selectedPost.url];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Erro na publicação :-(: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
    } else {
        
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"Corretor Tech", @"name",
                                       self.selectedPost.titulo, @"caption",
                                       self.selectedPost.url, @"link",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Erro na publicação :-(: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Compartilhado, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }
    
    
}


- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


@end

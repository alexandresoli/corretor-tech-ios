//
//  ImagensViewController.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Post;
@interface ImagensViewController : UIViewController

@property (weak, nonatomic) Post *selectedPost;
@property NSUInteger pageIndex;
@property BOOL isLastPage;

@end

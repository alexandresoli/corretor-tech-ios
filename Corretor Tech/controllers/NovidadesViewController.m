//
//  NovidadesViewController.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "NovidadesViewController.h"
#import "ImagensViewController.h"
#import "AppDelegate.h"
#import "Post.h"
#import "NSSet+Custom.h"

@interface NovidadesViewController () <UIPageViewControllerDataSource,UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *posts;
@property (nonatomic, strong) NSManagedObjectContext *moc;

@end

@implementation NovidadesViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    
    
    [self initMoc];
    
    [self configureView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self configureView];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *needRefresh = [defaults objectForKey:NEED_REFRESH_NEWS];
    
    if ([needRefresh boolValue]) {
        [self configureView];
        [defaults removeObjectForKey:NEED_REFRESH_NEWS];
        [defaults synchronize];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initMoc
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.moc = delegate.managedObjectContext;
}


#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    
    
    if ([updatedObjects containsMemberOfClass:[Post class]] ||
        [insertedObjects containsMemberOfClass:[Post class]] ||
        [deletedObjects containsMemberOfClass:[Post class]]) {

        [self configureView];
        
    }
    
}

- (void)configureView
{
//    self.posts = [CoreDataUtils fetchEntity:@"Post" withPredicate:[NSPredicate predicateWithFormat:@"categoria.descricao = 'Destaque' "] inContext:self.moc];
  
    self.posts = [CoreDataUtils fetchEntity:@"Post" withPredicate:[NSPredicate predicateWithFormat:@"categoria.descricao = 'Destaque' "] withKey:@"data" withOrder:NO withLimit:15 inContext:self.moc];
    
    if([self.posts count] > 0) {
        
        // Se o pageViewController já está carregado apenas reinicia os seus controllers.
        if (self.pageViewController) {
            
            ImagensViewController *startingViewController = [self viewControllerAtIndex:0];
            NSArray *viewControllers = @[startingViewController];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
        } else {
            // Instancia o UIPageViewController
            self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
            self.pageViewController.dataSource = self;
            
            
            // Inicia com o primeiro post.
            ImagensViewController *startingViewController = [self viewControllerAtIndex:0];
            NSArray *viewControllers = @[startingViewController];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
            // Change the size of page view controller
            self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            
            [self addChildViewController:self.pageViewController];
            [self.view addSubview:self.pageViewController.view];
            [self.pageViewController didMoveToParentViewController:self];
            
            // Busca nas subViews do pageViewController o seu pageControl para alterar a aparência.
            NSArray *subViews = [self.pageViewController.view subviews];
            for (UIView *view in subViews) {
                if ([view isKindOfClass:[UIPageControl class]]) {
                    UIPageControl *pageControl = (UIPageControl *)view;
                    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
                    pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:21.0/255.0 green:161.0/255.0 blue:225.0/255.0 alpha:1.0];
                    pageControl.backgroundColor = [UIColor clearColor];
                }
            }
        }
    }

    [self.pageViewController reloadInputViews];

}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = (((ImagensViewController *) viewController).pageIndex);
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = (((ImagensViewController *) viewController).pageIndex);
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.posts count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (ImagensViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.posts count] == 0) || (index >= [self.posts count])) {
        return nil;
    }
    
    ImagensViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ImagensViewController"];
    
    pageContentViewController.selectedPost = [self.posts objectAtIndex:index];
    
    
    pageContentViewController.pageIndex = index;
    
    if (index == [self.posts count] - 1) {
        pageContentViewController.isLastPage = YES;
    }
    
    return pageContentViewController;
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.posts count];
}


- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}



@end

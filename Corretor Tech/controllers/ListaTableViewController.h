//
//  ListaTableViewController.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListaTableViewController : UITableViewController


@property (nonatomic,strong) NSString *categoria;

@end

//
//  ListaTableViewController.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "ListaTableViewController.h"
#import "AppDelegate.h"
#import "Post.h"
#import "PostCell.h"
#import "Dateutils.h"
#import "PostViewController.h"
#import "NSSet+Custom.h"
#import "Updater.h"
#import "AFNetworking.h"
#import "SVPullToRefresh.h"


@interface ListaTableViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, strong) Post *selectedPost;
@property(nonatomic, strong) UIRefreshControl *refresh;

@end

@implementation ListaTableViewController;

@synthesize refresh;


#pragma mark - View Life Cycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView setSeparatorColor:[UIColor grayColor]];
    
    if(self.categoria == nil) {
        self.title = @"Todos";
        
    } else {
        
        if ([self.categoria isEqualToString:@"Mercado"]) {
            self.title = @"Mercado Imobiliário";
            
        } else if ([self.categoria isEqualToString:@"Redes Sociais"]) {
            self.title = @"Mídias Sociais";
            
        } else {
            self.title = self.categoria;
        }
    }


    [self initMoc];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    
    refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Solte para atualizar..."];
    refresh.tintColor = [UIColor colorWithRed:21.0/255.0 green:161.0/255.0 blue:225.0/255.0 alpha:0.5];
    [refresh addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged ];
    self.refreshControl = refresh;

    self.tableView.tableFooterView = [[UIView alloc] init];
    
    __weak ListaTableViewController *weakSelf = self;
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf loadOldPosts];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Init Moc

- (void)initMoc
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.moc = delegate.managedObjectContext;
}


#pragma mark - Get Background Moc

- (NSManagedObjectContext *)getBackgroundMoc
{
    NSManagedObjectContext *backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    backgroundContext.persistentStoreCoordinator = self.moc.persistentStoreCoordinator;
    backgroundContext.undoManager = nil;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSManagedObjectContextDidSaveNotification
                                                      object:backgroundContext
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification* note) {
                                                      
                                                      AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
                                                      
                                                      NSManagedObjectContext *mainMoc = delegate.managedObjectContext;
                                                      [mainMoc performBlock:^(){
                                                          [mainMoc mergeChangesFromContextDidSaveNotification:note];
                                                          [mainMoc save:nil];
                                                      }];
                                                  }];

    return backgroundContext;
}


#pragma mark - Refresh View

- (void)refreshView
{
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        return;
    }

    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Atualizando..."];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://www.corretortech.com.br/?json=get_recent_posts&page=1&count=20" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSManagedObjectContext *backgroundMoc = [self getBackgroundMoc];
            
            [backgroundMoc performBlock:^{
                [Updater convert:responseObject withMoc:backgroundMoc];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.moc save:nil];
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"MMM d, h:mm a"];
                    NSString *lastUpdated = [NSString stringWithFormat:@"Atualizado em %@",[formatter stringFromDate:[NSDate date]]];
                    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
                    
                    [refresh endRefreshing];
                    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Solte para atualizar..."];
                });
            }];
        });
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [refresh endRefreshing];
            refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Solte para atualizar..."];
        });
    }];
}


#pragma mark - Load Old Posts

- (void)loadOldPosts
{
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        [self.tableView.infiniteScrollingView stopAnimating];
        
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    __block NSNumber *lastPage = [userDefaults objectForKey:LAST_PAGE];
    int intLastPage;
    
    if (lastPage) {
        intLastPage = [lastPage intValue];
    } else {
        intLastPage = 1;
    }
    
    intLastPage++;
    
    NSString *url = [NSString stringWithFormat:@"http://www.corretortech.com.br/?json=get_recent_posts&page=%i&count=20", intLastPage];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSManagedObjectContext *backgroundMoc = [self getBackgroundMoc];
            
            [backgroundMoc performBlock:^{
                [Updater convert:responseObject withMoc:backgroundMoc];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.moc save:nil];
                    
                    [self.tableView reloadData];
                    [self.tableView.infiniteScrollingView stopAnimating];
                    
                    lastPage = [NSNumber numberWithInt:intLastPage];
                    
                    [userDefaults setObject:lastPage forKey:LAST_PAGE];
                    [userDefaults synchronize];
                });
            }];
        });
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView.infiniteScrollingView stopAnimating];
        });
    }];
}


#pragma mark - Notification Center

- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    
    
    if ([updatedObjects containsMemberOfClass:[Post class]] ||
        [insertedObjects containsMemberOfClass:[Post class]] ||
        [deletedObjects containsMemberOfClass:[Post class]]) {
        
        self.fetchedResultsController = nil;
        [self.fetchedResultsController performFetch:nil];
        [self.tableView reloadData];
        
    }
    
}


#pragma mark - NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    
    /*
	 Set up the fetched results controller.
     */
	// Create the fetch request for the entity.
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = nil;
    
    if(self.categoria != nil) {
        predicate = [NSPredicate predicateWithFormat:@"categoria.descricao = %@",self.categoria];
    }
    
	// Edit the entity name as appropriate.
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:self.moc];
    
    [fetchRequest setPredicate:predicate];
	[fetchRequest setEntity:entity];
    
	// Set the batch size to a suitable number.
	[fetchRequest setFetchBatchSize:20];
    
	// Sort using the timeStamp property.
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"data" ascending:NO];
	[fetchRequest setSortDescriptors:@[sortDescriptor ]];
    
    // Use the sectionIdentifier property to group into sections.
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.moc sectionNameKeyPath:@"titulo" cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    NSError* error;
    
    [_fetchedResultsController performFetch:&error];
    
	return _fetchedResultsController;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PostCell";
    
    PostCell *cell = (PostCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Post *post = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMMM, YYYY"];
    
    
    NSString *titulo = [post.titulo stringByReplacingOccurrencesOfString:@"&#8211;" withString:@"-"];
    cell.titulo.text = titulo;
    cell.data.text = [formatter stringFromDate:post.data];//[DateUtils stringFromDate:post.data];
    
    if(post.thumbnail == nil) {
        cell.imagem.image = [UIImage imageNamed:@"sem-imagem"];
    } else {
        cell.imagem.image = [UIImage imageWithData:post.thumbnail];
    }

    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = [UIColor colorWithRed:21.0/255.0 green:161.0/255.0 blue:225.0/255.0 alpha:1.0].CGColor;
    sublayer.frame = CGRectMake(cell.imagem.bounds.size.width, 0, 4, cell.imagem.frame.size.height);
    [cell.imagem.layer addSublayer:sublayer];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedPost = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self performSegueWithIdentifier:@"PostSegue" sender:self];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"PostSegue"]) {
        PostViewController *controller = segue.destinationViewController;
        controller.selectedPost = self.selectedPost;
    }
}


@end

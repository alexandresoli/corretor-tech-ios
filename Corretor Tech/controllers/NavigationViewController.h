//
//  NavigationViewController.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/26/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationViewController : UINavigationController

@end

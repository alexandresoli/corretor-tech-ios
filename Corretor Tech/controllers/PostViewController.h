//
//  PostViewController.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Post;
@interface PostViewController : UIViewController

@property (nonatomic,strong) Post *selectedPost;

@end

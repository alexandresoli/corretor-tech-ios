//
//  ImagensViewController.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "ImagensViewController.h"
#import "Post.h"
#import "Autor.h"
#import "Categoria.h"
#import "PostViewController.h"

@interface ImagensViewController () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *categoria;
@property (weak, nonatomic) IBOutlet UILabel *data;
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UILabel *autor;

@end

@implementation ImagensViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageView.image = [UIImage imageWithData:self.selectedPost.imagem];
    
    self.categoria.text = self.selectedPost.categoria.descricao;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMMM, YYYY"];
    
    self.data.text = [formatter stringFromDate:self.selectedPost.data];
    
    NSString *titulo = [self.selectedPost.titulo stringByReplacingOccurrencesOfString:@"&#8211;" withString:@"-"];
    self.titulo.text = titulo;
    self.autor.text = self.selectedPost.autor.nome;
    
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enter)];
    gesture.delegate = self;
    
    [self.imageView addGestureRecognizer:gesture];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)enter
{
    [self performSegueWithIdentifier:@"PostSegue" sender:self];
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"PostSegue"]) {
        PostViewController *controller = segue.destinationViewController;
        controller.selectedPost = self.selectedPost;
    }
    
    
}


@end

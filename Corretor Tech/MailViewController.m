//
//  MailViewController.m
//  BrEstate
//
//  Created by Alexandre Oliveira on 11/2/13.
//  Copyright (c) 2013 Alexandre Oliveira. All rights reserved.
//

#import "MailViewController.h"

@import MessageUI;

#import "DateUtils.h"
#import "Post.h"

@interface MailViewController ()  <MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) MFMailComposeViewController* mailComposer;

@property (nonatomic, strong) UIViewController *sender;

@end

@implementation MailViewController

@synthesize mailComposer;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}



#pragma mark - Cliente
- (void)sendEmail:(Post *)post forSender:(UIViewController *)sender withBody:(NSString *)body
{


    if([MFMailComposeViewController canSendMail])
    {
        
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        
        [mailComposer setSubject:post.titulo];
        [mailComposer setMessageBody:body isHTML:YES];
        [mailComposer setModalPresentationStyle:UIModalPresentationPageSheet];
        [mailComposer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        self.sender = sender;
        [self.sender.parentViewController presentViewController:mailComposer animated:YES completion:nil];
        
    } else
    {
        [self showMessage];
    }
    
}




#pragma mark - Warning Message
- (void)showMessage
{

    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Nenhuma conta de e-mail foi encontrada. Configure uma conta e tente novamente." delegate:nil cancelButtonTitle:@"Dispensar" otherButtonTitles:nil, nil];
    [alertView show];
}


#pragma mark - MFMailComposerDelegate Methods
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    if (result == MFMailComposeResultSent) {
        

        
    }
    
    [self.sender dismissViewControllerAnimated:YES completion:nil];
}

@end

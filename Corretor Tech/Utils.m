//
//  Utils.m
//  Corretor Tech
//
//  Created by Leonardo Barros on 25/08/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "Utils.h"
#import "AFNetworking.h"


@implementation Utils


#pragma mark - Show Alert

+ (void)showMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
        [alertView show];
    });
}


#pragma mark - isConnected

+ (BOOL)isConnected
{
    BOOL isConnected = [AFNetworkReachabilityManager sharedManager].reachable;
    return isConnected;
}


@end

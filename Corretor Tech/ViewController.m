//
//  ViewController.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "ViewController.h"

#import "AppDelegate.h"
#import "ListaTableViewController.h"
#import "SVProgressHUD.h"
#import "Updater.h"
#import "AFNetworking.h"


@interface ViewController ()

@property(nonatomic,weak) IBOutlet UIButton *mercadoButton;
@property(nonatomic,weak) IBOutlet UIButton *tecnologiaButton;
@property(nonatomic,weak) IBOutlet UIButton *midiasSociaisButton;
@property(nonatomic,weak) IBOutlet UIButton *negociosButton;
@property(nonatomic,weak) IBOutlet UIButton *todosButton;
@property(nonatomic,weak) IBOutlet UIView *containerView;

@property NSInteger selectedTag;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initMoc];
    
   // [Updater checkPosts:self.moc];
    
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self checkNeedLoadPosts];
}


- (void)initMoc
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.moc = delegate.managedObjectContext;
}


#pragma mark - Check Need Load Posts

- (void)checkNeedLoadPosts
{
    NSArray *postsArray = [CoreDataUtils fetchEntity:@"Post" withPredicate:nil inContext:self.moc];
    
    if (postsArray.count == 0) {
        [Updater checkPosts:self.moc];
    }
}


#pragma mark - IBAction
- (IBAction)showPosts:(UIButton *)sender
{
    
    UIButton *button  = sender;
    self.selectedTag = button.tag;
    
    [self performSegueWithIdentifier:@"ListaSegue" sender:self];
    
    
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ListaSegue"]) {
        
        NSString *categoria = nil;
        
        if(self.selectedTag == 1000) {
            categoria = @"Mercado";
        } else if(self.selectedTag == 2000) {
            categoria = @"Tecnologia";
        } else if(self.selectedTag == 3000) {
            categoria = @"Redes Sociais";
        } else if(self.selectedTag == 4000) {
            categoria = @"Negócios";
        }
        
        ListaTableViewController *controller = segue.destinationViewController;
        controller.categoria = categoria;
        
    }
}

@end

//
//  MailViewController.h
//  BrEstate
//
//  Created by Alexandre Oliveira on 11/2/13.
//  Copyright (c) 2013 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Post;

@interface MailViewController : UIViewController

- (void)sendEmail:(Post *)post forSender:(UIViewController *)sender withBody:(NSString *)body;

@end

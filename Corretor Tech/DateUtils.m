//
//  DateUtils.m
//  CatalogoLeisANS
//
//  Created by Alexandre Oliveira on 11/22/11.
//  Copyright (c) 2011 BRQ IT Services. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+(NSDate *) dateFromString:(NSString*) dateString withGMT:(int) GMT
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:GMT]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    return [dateFormatter dateFromString:dateString];
}

+(NSDate *) dateFromString:(NSString*) dateString withFormat:(NSString *) format
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    
    if (!dateFormatter) {
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    } else {
        [dateFormatter setDateFormat:format];
    }
    
    
    return [dateFormatter dateFromString:dateString];
}

+ (NSString *) stringFromDate:(NSDate *)date
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    return [dateFormatter stringFromDate:date];
    
}

+(NSString *) stringFromDate:(NSDate *) date withFormat:(NSString *) format
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
    
    
    if (!format) {
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    } else {
        [dateFormatter setDateFormat:format];
    }
    
    
    return [dateFormatter stringFromDate:date];
}

+ (NSInteger)currentDay
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    NSDateComponents *components = [calendar components:NSDayCalendarUnit fromDate:today];
    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    
    return [components day];
}

+ (NSInteger)currentMonth
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    
    NSDateComponents *components = [calendar components:NSMonthCalendarUnit fromDate:today];
    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    
    
    return [components month];
}

+ (NSInteger)currentYear
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:today];
    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    
    
    return  [components year];
}


+ (NSString *)completeHour
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    
    NSDateComponents *components = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit  fromDate:today];
    long hour = [components hour];
    long minutes = [components minute];
    
    NSString *completeHour = [NSString stringWithFormat:@"%ld:%ld",hour,minutes];
    
    return completeHour;
}

+ (NSString *)completeHourFromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    NSDateComponents *components = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit  fromDate:date];
    //    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    long hour = [components hour];
    NSMutableString *minutes = [NSMutableString stringWithFormat:@"%ld",(long)[components minute]];
    
    if ([minutes isEqualToString:@"0"]) {
        [minutes stringByAppendingString:@"0"];
    } else if(minutes.length == 1) {
        
        [minutes insertString:@"0" atIndex:0];
    }
    
    
    NSString *completeHour = [NSString stringWithFormat:@"%ld:%@",hour,minutes];
    
    return completeHour;
}

+ (NSString *)fullDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    NSString *fullDate = [dateFormatter stringFromDate:date];
    //     NSString *now = [[NSDate date] descriptionWithLocale:currentLocale];
    return fullDate;
}

+ (NSString *)monthYearDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    
    
    [dateFormatter setCalendar:[NSCalendar currentCalendar]];
    
    NSString *formatTemplate = [NSDateFormatter dateFormatFromTemplate:@"MMMM YYYY" options:0 locale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:formatTemplate];
    
    NSString *monthYear = [dateFormatter stringFromDate:date];
    
    return monthYear;
}

+ (NSDate *)dateWithDays:(NSUInteger)days
{
    NSDateComponents *dayComponent = [NSDateComponents new];
    [dayComponent setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    dayComponent.day = days;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    NSDate *dateToBeIncremented = [NSDate date];
    NSDate *newDate = [calendar dateByAddingComponents:dayComponent toDate:dateToBeIncremented options:0];
    
    return newDate;
}


+ (NSDate *)today
{
    NSDateComponents *today = [NSDateComponents new];
    [today setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    
    today.calendar = [NSCalendar currentCalendar];
    
    today.year = [DateUtils currentYear];
    today.month = [DateUtils currentMonth];
    today.day = [DateUtils currentDay];
    
    [today setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    
    return [today date];
}

+ (NSDate *)lastMinuteFromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3]];
    NSDateComponents *component = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    component.hour = 23;
    component.minute = 59;
    component.second = 59;
    NSDate *newDate = [calendar dateFromComponents:component];
    
    return newDate;
}

+ (NSString *)timeFromDate:(NSDate *)date
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"HH:mm";
    fmt.timeZone = [NSTimeZone localTimeZone];
    
    NSString *strDate = [fmt stringFromDate:date];
    
    return strDate;
}

+ (NSDate *)dateByAddingHours:(NSDate *)date hours:(int)hours
{
    NSTimeInterval seconds = hours * 60 * 60;
    NSDate *newDate = [date dateByAddingTimeInterval:seconds];
    return newDate;
    
}

+ (NSComparisonResult)compare:(NSDate *)date1 withDate:(NSDate *)date2
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    
    NSString *strDate1 = [formatter stringFromDate:date1];
    NSString *strDate2 = [formatter stringFromDate:date2];
    
    
    date1 = [formatter dateFromString:strDate1];
    date2 = [formatter dateFromString:strDate2];
    
    
    NSComparisonResult result = [date1 compare:date2];
    
    
    return result;
}
@end

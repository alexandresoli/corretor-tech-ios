//
//  DateUtils.h
//  CatalogoLeisANS
//
//  Created by Alexandre Oliveira on 11/22/11.
//  Copyright (c) 2011 BRQ IT Services. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Responsável pelas operações envolvendo objetos NSDate */
@interface DateUtils : NSObject

/**---------------------------------------------------------------------------------------
 * @name Métodos
 *  ---------------------------------------------------------------------------------------
 */

/** Método utilizado para converter uma NSString em um objeto NSDate fazendo uso do GMT.
 
 @param dateString String a ser convertida.
 @param GMT Greenwich Mean Time.
 @return objeto NSDate contendo a data convertida.
 */
+(NSDate *) dateFromString:(NSString *) dateString withGMT:(int) GMT;

/** Método utilizado para converter uma NSString em um objeto NSDate utilizando um formato específico.
 
 @param dateString String a ser convertida.
 @param format formato a ser utilizado na conversão.
 @return objeto NSDate contendo a data convertida.
 */
+(NSDate *) dateFromString:(NSString *) dateString withFormat:(NSString *) format;

/** Método utilizado para converter um objeto NSDate em um objeto NSString utilizando um formato específico.
 
 @param date NSDate a ser convertida em NSString.
 @param format formato a ser utilizado na conversão.
 @return objeto NSString contendo a data convertida.
 */
+(NSString *) stringFromDate:(NSDate *)date withFormat:(NSString *)format;

+ (NSInteger)currentDay;
+ (NSInteger)currentMonth;
+ (NSInteger)currentYear;
+ (NSString *)completeHour;
+ (NSString *)completeHourFromDate:(NSDate *)date;
+ (NSString *)fullDate:(NSDate *)date;
+ (NSString *)monthYearDate:(NSDate *)date;
+ (NSDate *)dateWithDays:(NSUInteger)days;
+ (NSDate *)today;
+ (NSDate *)lastMinuteFromDate:(NSDate *)date;
+ (NSString *)timeFromDate:(NSDate *)date;
+ (NSDate *)dateByAddingHours:(NSDate *)date hours:(int)hours;
+ (NSComparisonResult)compare:(NSDate *)date1 withDate:(NSDate *)date2;
+ (NSString *) stringFromDate:(NSDate *)date;

@end

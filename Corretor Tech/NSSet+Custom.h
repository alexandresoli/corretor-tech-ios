//
//  NSSet+Custom.h
//  BrEstate
//
//  Created by Alexandre Oliveira on 11/3/13.
//  Copyright (c) 2013 Alexandre Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSSet (Custom)

- (BOOL)containsKindOfClass:(Class)class;
- (BOOL)containsMemberOfClass:(Class)class;

@end

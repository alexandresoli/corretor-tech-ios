//
//  Utils.h
//  Corretor Tech
//
//  Created by Leonardo Barros on 25/08/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (void)showMessage:(NSString *)message;
+ (BOOL)isConnected;

@end

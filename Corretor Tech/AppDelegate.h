//
//  AppDelegate.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator ;
@property (strong, nonatomic) NSDictionary *refererAppLink;
@end

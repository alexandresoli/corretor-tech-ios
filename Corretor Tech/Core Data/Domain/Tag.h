//
//  Tag.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post;

@interface Tag : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSNumber * contador;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSSet *post;
@end

@interface Tag (CoreDataGeneratedAccessors)

- (void)addPostObject:(Post *)value;
- (void)removePostObject:(Post *)value;
- (void)addPost:(NSSet *)values;
- (void)removePost:(NSSet *)values;

@end

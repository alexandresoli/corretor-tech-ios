//
//  Tag.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "Tag.h"
#import "Post.h"


@implementation Tag

@dynamic codigo;
@dynamic contador;
@dynamic descricao;
@dynamic post;

@end

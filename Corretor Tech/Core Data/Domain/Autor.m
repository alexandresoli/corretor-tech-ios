//
//  Autor.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "Autor.h"
#import "Post.h"


@implementation Autor

@dynamic codigo;
@dynamic descricao;
@dynamic nome;
@dynamic url;
@dynamic post;

@end

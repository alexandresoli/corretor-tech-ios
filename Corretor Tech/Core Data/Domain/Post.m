//
//  Post.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "Post.h"
#import "Autor.h"
#import "Categoria.h"
#import "Tag.h"


@interface Post ()

@property (nonatomic) NSDate *primitiveData;
@property (nonatomic) NSString *primitiveSectionIdentifier;

@end

@implementation Post

@dynamic codigo;
@dynamic conteudo;
@dynamic data;
@dynamic imagem;
@dynamic modificado;
@dynamic resumo;
@dynamic status;
@dynamic thumbnail;
@dynamic titulo;
@dynamic url;
@dynamic autor;
@dynamic categoria;
@dynamic tags;
@dynamic sectionIdentifier;
@dynamic primitiveData;
@dynamic primitiveSectionIdentifier;



#pragma mark - Transient properties

- (NSString *)sectionIdentifier
{
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"sectionIdentifier"];
    NSString *tmp = [self primitiveSectionIdentifier];
    [self didAccessValueForKey:@"sectionIdentifier"];
    
    if (!tmp)
    {
        /*
         Sections are organized by day, month and year. Create the section identifier as a string representing the number (year * 1000) + (month * 100) + day; this way they will be correctly ordered chronologically regardless of the actual name of the month.
         */
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[self data]];
        tmp = [NSString stringWithFormat:@"%ld", (long)([components year] * 1000) + ([components month] * 100) + [components day] ];
        [self setPrimitiveSectionIdentifier:tmp];
    }
    return tmp;
}


#pragma mark - Data setter
- (void)setData:(NSDate *)data
{
    // If data inicio changes, the section identifier become invalid.
    [self willChangeValueForKey:@"data"];
    [self setPrimitiveData:data];
    [self didChangeValueForKey:@"data"];
    
    [self setPrimitiveSectionIdentifier:nil];
}


#pragma mark - Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier
{
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"data"];
}




@end

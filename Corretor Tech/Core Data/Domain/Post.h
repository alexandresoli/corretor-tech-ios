//
//  Post.h
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Autor, Categoria, Tag;

@interface Post : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * conteudo;
@property (nonatomic, retain) NSDate * data;
@property (nonatomic, retain) NSData * imagem;
@property (nonatomic, retain) NSDate * modificado;
@property (nonatomic, retain) NSString * resumo;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) Autor *autor;
@property (nonatomic, retain) Categoria *categoria;
@property (nonatomic, retain) NSSet *tags;
@property (nonatomic, retain) NSString * sectionIdentifier;

@end

@interface Post (CoreDataGeneratedAccessors)

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

@end

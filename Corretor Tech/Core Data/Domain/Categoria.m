//
//  Categoria.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/25/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "Categoria.h"
#import "Post.h"


@implementation Categoria

@dynamic codigo;
@dynamic contador;
@dynamic descricao;
@dynamic post;

@end

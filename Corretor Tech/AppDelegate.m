//
//  AppDelegate.m
//  Corretor Tech
//
//  Created by Alexandre Oliveira on 4/24/14.
//  Copyright (c) 2014 BrEstate. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "Updater.h"
#import <GAI.h>
#import "TestFlight.h"
#import "NSFileManager+DoNotBackup.h"
#import "AFNetworking.h"


@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    //[[UILabel appearance] setFont:[UIFont fontWithName:@"DINAlternate-Bold" size:15.0]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIFont fontWithName:@"DINAlternate-Bold" size:17.0],NSFontAttributeName, nil]];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont fontWithName:@"DINAlternate-Bold" size:15.0]}
                                                                                            forState:UIControlStateNormal];
    
    
    UINavigationController *navController = [self getRootViewController];
    self.window.rootViewController = navController;
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    NSLog(@"Launched in background %d", UIApplicationStateBackground == application.applicationState);
    
    
    // Google Analytics
    [self initGoogleAnalytics];
    
    // Testflight
    [TestFlight takeOff:@"263bdf48-15c3-4ff4-befa-a0f9c2072575"];

    [self excludeFromIcloudBackup];
    
    // Permission for notification on iOS 8.
    if (IS_OS_8_OR_LATER) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    // Check if the app was open by a notification.
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif) {
        [navController.childViewControllers.firstObject performSegueWithIdentifier:@"ListaSegue" sender:nil];
    }
    
    return YES;
}


- (void)initGoogleAnalytics
{
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 120;
    
    // Optional: set Logger to VERBOSE for debug information.
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    
    UIDevice *currentDevice = [UIDevice currentDevice];
    if ([currentDevice.model rangeOfString:@"Simulator"].location == NSNotFound) {
        
        // Initialize tracker.
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-39963498-3"];
        //[[GAI sharedInstance] defaultTracker];
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Get Root View Controller

- (UINavigationController *)getRootViewController
{
    ViewController *rootController;
    
    if ([ [ UIScreen mainScreen ] bounds ].size.height == 568) {
        rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"iPhone5Menu"];
    } else {
        rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"iPhone4Menu"];
    }
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:rootController];
    navController.navigationBar.tintColor = [UIColor colorWithRed:21.0/255.0 green:161.0/255.0 blue:225.0/255.0 alpha:1.0];
    
    return navController;
}


#pragma mark - Core Data Stack
// 1
- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}

//2
- (NSManagedObjectModel *)managedObjectModel {
    
    
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Model" ofType:@"momd"];
    NSURL *momURL = [NSURL fileURLWithPath:path];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
    
    return _managedObjectModel;
}

//3
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
//    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* documentsPath = [self applicationDocumentsDirectory];
    NSError *error = nil;
    
    NSString* dbPath = [documentsPath stringByAppendingFormat:@"/Model.sqlite"];
//    NSString* defaultDB = [[NSBundle mainBundle] pathForResource:@"DefaultDB" ofType:@"sqlite"];
    
    
//    if ([fileManager fileExistsAtPath:defaultDB]) {
//        
//        if (![fileManager fileExistsAtPath:dbPath]) {
//            [fileManager copyItemAtPath:defaultDB toPath:dbPath error:&error];
//            
//            if (error != nil) {
//                NSLog(@"%@",[error description]);
//            }
//        }
//        
//    }
    
    NSURL *storeURL = [NSURL fileURLWithPath:dbPath];
    
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
    						 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
    						 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // handle error
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


#pragma mark - Background Fetch

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    
    NSURL *url = [[NSURL alloc] initWithString:@"http://www.corretortech.com.br/?json=get_recent_posts&page=1"];
    
    NSURLSessionDataTask *task = [session dataTaskWithURL:url
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                            
                                            if (error) {
                                                completionHandler(UIBackgroundFetchResultFailed);
                                                return;
                                            }
                                            
                                            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                            
                                            BOOL hasNewData = [Updater hasNewData:json withMoc:_managedObjectContext];
                                            
                                            
                                            if (hasNewData) {
                                                int newPosts = [Updater convert:json withMoc:_managedObjectContext];
                                                [self presentNotification:newPosts];
                                                
                                                completionHandler(UIBackgroundFetchResultNewData);
                                                
                                            } else {
                                                completionHandler(UIBackgroundFetchResultNoData);
                                            }
                                        }];
    
    // Start the task
    [task resume];
}


#pragma mark - Receive Local Notification

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notif
{
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        UINavigationController *navController = [self getRootViewController];
        self.window.rootViewController = navController;
        
        [navController.childViewControllers.firstObject performSegueWithIdentifier:@"ListaSegue" sender:nil];
    }
}


#pragma mark - Notification

-(void)presentNotification:(int)newPosts
{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.alertBody = @"Novos posts estão disponíveis!";
    localNotification.alertAction = @"Visualizar";
    
    //On sound
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    //increase the badge number of application plus 1
    localNotification.applicationIconBadgeNumber = newPosts;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NEED_REFRESH_NEWS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Facebook SDK
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    BOOL urlWasHandled =
    [FBAppCall handleOpenURL:url
           sourceApplication:sourceApplication
             fallbackHandler:
     ^(FBAppCall *call) {
         // Parse the incoming URL to look for a target_url parameter
         NSString *query = [url query];
         NSDictionary *params = [self parseURLParams:query];
         // Check if target URL exists
         NSString *appLinkDataString = [params valueForKey:@"al_applink_data"];
         if (appLinkDataString) {
             NSError *error = nil;
             NSDictionary *applinkData =
             [NSJSONSerialization JSONObjectWithData:[appLinkDataString dataUsingEncoding:NSUTF8StringEncoding]
                                             options:0
                                               error:&error];
             if (!error &&
                 [applinkData isKindOfClass:[NSDictionary class]] &&
                 applinkData[@"target_url"]) {
                 self.refererAppLink = applinkData[@"referer_app_link"];
                 NSString *targetURLString = applinkData[@"target_url"];
                 // Show the incoming link in an alert
                 // Your code to direct the user to the
                 // appropriate flow within your app goes here
                 [[[UIAlertView alloc] initWithTitle:@"Received link:"
                                             message:targetURLString
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
             }
         }
     }];
    
    return urlWasHandled;
}

// A function for parsing URL parameters
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}


#pragma mark - Avoid iCloud Backup
- (void)excludeFromIcloudBackup
{
    //    NSString *dbPath = [[self applicationDocumentsDirectory] stringByAppendingFormat:@"/Model.sqlite"];
    //    NSString *shmPath = [[self applicationDocumentsDirectory] stringByAppendingFormat:@"/Model.sqlite-shm"];
    //    NSString *wallPath = [[self applicationDocumentsDirectory] stringByAppendingFormat:@"/Model.sqlite-wal"];
    //
    //    NSURL *dbURL = [NSURL fileURLWithPath:dbPath];
    //    NSURL *shmURL = [NSURL fileURLWithPath:shmPath];
    //    NSURL *wallURL = [NSURL fileURLWithPath:wallPath];
    
    //    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:dbURL];
    //    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:shmURL];
    //    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:wallURL];
    
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:documentsURL];
    
}

@end
